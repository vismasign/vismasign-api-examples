<?php

use GuzzleHttp\Middleware;

// Change these
const CLIENT_IDENTIFIER = "ddf58116-6082-4bfc-a775-0c0bb2f945ce";
const SECRET_KEY = "jp7SjOOr4czRTifCo30qx0sZAIw9PW+vVpsbP09pQaY=";

function authorizationHeader(
    $username,
    $secret,
    $method,
    $body,
    $contentType,
    $date,
    $path
) {
    $str = join(
        "\n",
        [
            $method,
            base64_encode(hash('md5', $body, true)),
            $contentType,
            $date,
            $path
        ]
    );

    $header = 'Onnistuu ' . $username . ':' . base64_encode(
            hash_hmac(
                'sha512',
                $str,
                $secret,
                true
            )
        );

    return $header;
}

return Middleware::mapRequest(function (\GuzzleHttp\Psr7\Request $request) {
    return $request->withHeader(
        'Authorization',
        authorizationHeader(
            CLIENT_IDENTIFIER,
            base64_decode(SECRET_KEY),
            $request->getMethod(),
            (string)$request->getBody(),
            ($request->getHeader('Content-Type') ?: [''])[0],
            $request->getHeader('Date')[0],
            $request->getUri()->getPath() .
                ($request->getUri()->getQuery() ? '?' . $request->getUri()->getQuery() : '')
        )
    );
});
