<?php

require_once('vendor/autoload.php');

$authMiddleware = include('api-request.php');

$client = new \GuzzleHttp\Client();
$clientHandler = $client->getConfig('handler');

$response = $client->request(
    'POST',
    'https://sign.visma.net/api/v1/document/',
    [
        'json' => [
            'document' => [
                'name' => 'Test document ' . date(\DateTime::ATOM),
            ]
        ],
        'headers' => [
            'Date' => date('r'),
        ],
        'handler' => $authMiddleware($clientHandler),
    ]
);

if ($response->getStatusCode() !== 201) {
    throw new Exception('Could not create document');
}

$document = $response->getHeader('Location')[0];


$response = $client->request(
    'POST',
    $document . '/files',
    [
        'body' => file_get_contents('https://sign.visma.net/example_2page.pdf'),
        'headers' => [
            'Date' => date('r'),
            'Content-Type' => 'application/pdf',
        ],
        'handler' => $authMiddleware($clientHandler),
    ]
);

if ($response->getStatusCode() !== 201) {
    throw new Exception('Could not add file to document');
}

// Note this is an array of requirements
$requirements = [
    [
        'email' => 'petri.koivula@fraktio.fi',
    ]
];

$response = $client->request(
    'POST',
    $document . '/invitations',
    [
        'json' => $requirements,
        'headers' => [
            'Date' => date('r'),
        ],
        'handler' => $authMiddleware($clientHandler),
    ]
);

if ($response->getStatusCode() !== 201) {
    throw new Exception('Could not create invitations');
}

echo "<pre>" . json_encode(json_decode($response->getBody()->getContents(), true), JSON_PRETTY_PRINT);

$response = $client->request(
    'GET',
    $document,
    [
        'headers' => [
            'Date' => date('r'),
            'Content-Type' => 'application/json',
        ],
        'handler' => $authMiddleware($clientHandler),
    ]
);

if ($response->getStatusCode() !== 200) {
    throw new Exception('Could not fetch document');
}

echo PHP_EOL . PHP_EOL . json_encode(json_decode($response->getBody()->getContents(), true), JSON_PRETTY_PRINT);

$response = $client->request(
    'GET',
    'https://sign.visma.net/api/v1/document/?name=test',
    [
        'headers' => [
            'Date' => date('r'),
        ],
        'handler' => $authMiddleware($clientHandler),
    ]
);

if ($response->getStatusCode() !== 200) {
    throw new Exception('Could not search for document');
}

echo PHP_EOL . PHP_EOL . json_encode(json_decode($response->getBody()->getContents(), true), JSON_PRETTY_PRINT);
