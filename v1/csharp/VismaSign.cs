﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;

namespace visma_sign_csharp
{
    public class VismaSign
    {
        private string root;
        private string identifier;
        private HashAlgorithm macHash;
        private HashAlgorithm contentHash;
        private HttpClient client = new HttpClient();

        public VismaSign(string root, string identifier, string secret)
        {
            this.root = root;
            this.identifier = identifier;
            this.macHash = new HMACSHA512(Convert.FromBase64String(secret));
            this.contentHash = new MD5CryptoServiceProvider();
        }

        private async Task<HttpRequestMessage> WithAuthHeaders(HttpRequestMessage request)
        {
            if (request.Content != null) {
                request.Content.Headers.ContentMD5 = contentHash.ComputeHash(await request.Content.ReadAsByteArrayAsync());
            }

            request.Headers.Date = request.Headers.Date.GetValueOrDefault(DateTime.UtcNow);

            request.Headers.Authorization =
               new AuthenticationHeaderValue(
                   "Onnistuu",
                   identifier + ":" +
                    Convert.ToBase64String(
                        macHash.ComputeHash(
                            System.Text.Encoding.UTF8.GetBytes(
                                String.Join(
                                    "\n",
                                    new string[] {
                                        request.Method.ToString(),
                                        Convert.ToBase64String(
                                            request.Content != null ?
                                            request.Content.Headers.ContentMD5 :
                                            contentHash.ComputeHash(new byte[] {})
                                        ),
                                        request.Content != null ? request.Content.Headers.ContentType.ToString() : "",
                                        request.Headers.Date.GetValueOrDefault(DateTime.UtcNow).ToString("r"),
                                        request.RequestUri.ToString().Replace(root, "")
                                    }
                                )
                            )
                        )
                    )
                );

            return request;
        }

        public async Task<string> DocumentCreate(string body)
        {
            var request = new HttpRequestMessage(
                HttpMethod.Post,
                root + "/api/v1/document/"
            );

            request.Content = new StringContent(body, System.Text.Encoding.UTF8, "application/json");
            request = await WithAuthHeaders(request);

            var response = await client.SendAsync(request);

            if (!response.IsSuccessStatusCode)
            {
                throw new Exception("Document create failed");
            }

            return response.Headers.Location.ToString();
        }

        public async Task DocumentAddFile(string documentUri, byte[] fileContent, string filename = null)
        {
            var request = new HttpRequestMessage(
                HttpMethod.Post,
                documentUri + "/files" +
                (
                    filename != null ?
                    ("?filename=" + HttpUtility.UrlEncode(filename)) :
                    ""
                )
            );

            var requestContent = new ByteArrayContent(fileContent);
            requestContent.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
            request.Content = requestContent;
            request = await WithAuthHeaders(request);

            var response = await client.SendAsync(request);

            if (!response.IsSuccessStatusCode)
            {
                throw new Exception("Document file add failed");
            }
        }

        public async Task<string> DocumentAddInvitations(string documentUri, string body)
        {
            var request = new HttpRequestMessage(
                HttpMethod.Post,
                documentUri + "/invitations"
            );

            request.Content = new StringContent(body, System.Text.Encoding.UTF8, "application/json");
            request = await WithAuthHeaders(request);

            var response = await client.SendAsync(request);

            if (!response.IsSuccessStatusCode)
            {
                throw new Exception("Document add invitations failed");
            }

            return await response.Content.ReadAsStringAsync();
        }

        public async Task<string> DocumentSearch(string query)
        {
            var request = await WithAuthHeaders(new HttpRequestMessage(
                HttpMethod.Get,
                root + "/api/v1/document/" + (query != null ? "?" + query : "")
            ));

            var response = await client.SendAsync(request);

            if (!response.IsSuccessStatusCode) {
                throw new Exception("Document search failed");
            }

            return await response.Content.ReadAsStringAsync();
        }

        public async Task<string> DocumentGet(string documentUri)
        {
            var request = await WithAuthHeaders(new HttpRequestMessage(
                HttpMethod.Get,
                documentUri
            ));

            var response = await client.SendAsync(request);

            if (!response.IsSuccessStatusCode)
            {
                throw new Exception("Document get failed");
            }

            return await response.Content.ReadAsStringAsync();
        }
    }
}
