﻿using System;
using System.Threading.Tasks;
using System.Net;

namespace visma_sign_csharp
{
    class Program
    {
        static void Main(string[] args)
        {
            RunAsync().GetAwaiter().GetResult();
        }

        static async Task RunAsync()
        {
            var vismaSign = new VismaSign(
                "https://sign.visma.net",
                identifier: "", // uuid
                secret: "" // in base64
            );

            var documentUri = await vismaSign.DocumentCreate(
                "{\"document\":{\"name\":\"C# Test document\"}}"
            );
            Console.WriteLine("Created document: " + documentUri);

            WebClient client = new WebClient();
            byte[] fileContent = client.DownloadData("https://sign.visma.net/empty.pdf");
            await vismaSign.DocumentAddFile(documentUri, fileContent);
            Console.WriteLine("Added file to document");

            var invitations = await vismaSign.DocumentAddInvitations(
                documentUri,
                "[{\"email\":\"petri.koivula@fraktio.fi\"}]"
            );
            Console.WriteLine("Created invitations: " + invitations);

            var documents = await vismaSign.DocumentSearch("offset=0");
            Console.WriteLine(documents);
        }
    }
}
