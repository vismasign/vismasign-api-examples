
const rp = require("request-promise-native");
const VismaSign = require('./VismaSign');

// Fill api credentials here
const baseUrl = "https://sign.visma.net/";
const identifier = "";
const secret = Buffer.from("", "base64");

const vismaSign = new VismaSign(baseUrl, identifier, secret);

(async function () {
    const document = {document: {name: "Test"}};
    const fileContent = await rp({url: 'https://sign.visma.net/example_2page.pdf', encoding: null});
    const invitations = [
        {
            email: "petri.koivula@fraktio.fi"
        }
    ];
    
    const documentUuid = await vismaSign.createDocument(document);
    console.log("Created document", documentUuid);
    
    const fileUuid = await vismaSign.addFile(documentUuid, fileContent, "file-name-not-required.pdf");
    console.log("Added file", fileUuid);
    
    const createdInvitations = await vismaSign.createInvitations(documentUuid, invitations);
    console.log("Created invitations", createdInvitations);

    //console.log("Document", await vismaSign.getDocument(documentUuid));
    //console.log("Invitation", await vismaSign.getInvitation(createdInvitations[0].uuid));
    //console.log("Document file length", (await vismaSign.getDocumentFile(documentUuid)).length);

    //console.log("Cancel document", await vismaSign.cancelDocument(documentUuid));
    //console.log("Document", await vismaSign.getDocument(documentUuid));

    //console.log("Cancel document", await vismaSign.cancelDocument(documentUuid));
    //console.log("Delete document", await vismaSign.deleteDocument(documentUuid)); // Must be canceled or signed to delete
    //console.log("Document", await vismaSign.getDocument(documentUuid));

    //console.log("Auth methods", await vismaSign.getAuthMethods());

    //console.log("Fulfill send customer to", await vismaSign.fulfillInvitation(createdInvitations[0].uuid, 'https://my.service.tunk.io/return/some-identifier', '010101-123N', 'tupas-aktia'));

    //console.log("Remind document", await vismaSign.remindDocument(documentUuid));

    //console.log("Remind invitation", await vismaSign.remindInvitation(createdInvitations[0].uuid));

    //console.log("Search documents", await vismaSign.searchDocuments({status: 'signed'}));

    //console.log("Get categories", await vismaSign.getCategories());

    //const categoryUuid = await vismaSign.createCategory({name: "Test category"});
    //console.log("Create category", categoryUuid);
    //console.log("Update category", await vismaSign.updateCategory(categoryUuid, {name: "Another name"}));
    //console.log("Delete category", await vismaSign.deleteCategory(categoryUuid));

    //console.log("Get invitee groups", await vismaSign.getInviteeGroups());
    //console.log("Get saved email messages", await vismaSign.getSavedEmailMessages());
    //console.log("Get saved sms messages", await vismaSign.getSavedSmsMessages());
}());
