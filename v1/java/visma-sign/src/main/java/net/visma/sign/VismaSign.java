package net.visma.sign;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class VismaSign {
    private String baseUrl;
    private String identifier;
    private byte[] secret;

    public VismaSign(String baseUrl, String identifier, byte[] secret) {
        this.baseUrl = baseUrl;
        this.identifier = identifier;
        this.secret = secret;
    }

    private HttpURLConnection connection(String path, String method) throws IOException {
        URL url = new URL(this.baseUrl + path);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod(method);
        connection.setDoOutput(true);
        connection.setDoInput(true);
        connection.setUseCaches(false);
        connection.setInstanceFollowRedirects(false);
        connection.setRequestProperty("Content-Type", "application/json");
        return connection;
    }

    private void addAuthentication(HttpURLConnection connection, String path, byte[] body) throws NoSuchAlgorithmException, InvalidKeyException, IllegalStateException, UnsupportedEncodingException {
        MessageDigest md5 = MessageDigest.getInstance("MD5");
        SecretKeySpec secretKeySpec = new SecretKeySpec(this.secret, "HmacSHA512");
        Mac mac = Mac.getInstance("HmacSHA512");
        mac.init(secretKeySpec);

        connection.setRequestProperty("Content-MD5", Base64.getEncoder().encodeToString(md5.digest(body)));
        connection.setRequestProperty("Date", (new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z")).format(new Date()));

        connection.setRequestProperty(
                "Authorization",
                "Onnistuu " + this.identifier + ":" +
                        Base64.getEncoder().encodeToString(
                                mac.doFinal(
                                        String.join(
                                                "\n",
                                                new String[]{
                                                        connection.getRequestMethod(),
                                                        connection.getRequestProperty("Content-MD5"),
                                                        connection.getRequestProperty("Content-Type"),
                                                        connection.getRequestProperty("Date"),
                                                        path
                                                }
                                        ).getBytes("UTF-8")
                                )
                        )
        );
    }

    private byte[] responseBody(HttpURLConnection connection) throws IOException {
        InputStream is = connection.getInputStream();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] byteChunk = new byte[4096];
        int n;
        while ((n = is.read(byteChunk)) > 0) {
            baos.write(byteChunk, 0, n);
        }
        return baos.toByteArray();
    }

    public String createDocument(String document) throws IOException, NoSuchAlgorithmException, InvalidKeyException {
        String path = "/api/v1/document/";

        HttpURLConnection connection = this.connection(path, "POST");
        this.addAuthentication(connection, path, document.getBytes("UTF-8"));

        OutputStream outputStream = connection.getOutputStream();
        outputStream.write(document.getBytes("UTF-8"));
        outputStream.close();

        if (connection.getResponseCode() != 201) {
            throw new RuntimeException("Could not create document");
        }

        String[] locationParts = connection.getHeaderField("Location").split("/");
        return locationParts[locationParts.length - 1];
    }

    public String addFile(String documentUuid, byte[] fileContent, String fileName) throws IOException, InvalidKeyException, NoSuchAlgorithmException, IllegalStateException {
        String path = "/api/v1/document/" + documentUuid + "/files";

        HttpURLConnection connection = this.connection(path, "POST");
        connection.setRequestProperty("Content-Type", "application/pdf");
        this.addAuthentication(connection, path, fileContent);

        OutputStream outputStream = connection.getOutputStream();
        outputStream.write(fileContent);
        outputStream.close();

        if (connection.getResponseCode() != 201) {
            throw new RuntimeException("Could not add file");
        }

        return new String(this.responseBody(connection), "UTF-8");
    }

    public String createInvitations(String documentUuid, String invitations) throws IOException, NoSuchAlgorithmException, InvalidKeyException {
        String path = "/api/v1/document/" + documentUuid + "/invitations";

        HttpURLConnection connection = this.connection(path, "POST");
        this.addAuthentication(connection, path, invitations.getBytes("UTF-8"));

        OutputStream outputStream = connection.getOutputStream();
        outputStream.write(invitations.getBytes("UTF-8"));
        outputStream.close();

        if (connection.getResponseCode() != 201) {
            throw new RuntimeException("Could not create invitations");
        }

        return new String(this.responseBody(connection), "UTF-8");
    }

    public String getInvitation(String uuid) throws IOException, NoSuchAlgorithmException, InvalidKeyException {
        String path = "/api/v1/invitation/" + uuid;

        HttpURLConnection connection = this.connection(path, "GET");
        this.addAuthentication(connection, path, new byte[0]);

        if (connection.getResponseCode() != 200) {
            throw new RuntimeException("Could not get invitation");
        }

        return new String(this.responseBody(connection), "UTF-8");
    }

    public String getDocument(String uuid) throws IOException, NoSuchAlgorithmException, InvalidKeyException {
        String path = "/api/v1/document/" + uuid;

        HttpURLConnection connection = this.connection(path, "GET");
        this.addAuthentication(connection, path, new byte[0]);

        if (connection.getResponseCode() != 200) {
            throw new RuntimeException("Could not get document");
        }

        return new String(this.responseBody(connection), "UTF-8");
    }

    public byte[] getDocumentFile(String uuid) throws IOException, NoSuchAlgorithmException, InvalidKeyException {
        String path = "/api/v1/document/" + uuid + "/files/0";

        HttpURLConnection connection = this.connection(path, "GET");
        this.addAuthentication(connection, path, new byte[0]);

        if (connection.getResponseCode() != 200) {
            throw new RuntimeException("Could not get document file");
        }

        return this.responseBody(connection);
    }

    public void cancelDocument(String uuid) throws IOException, NoSuchAlgorithmException, InvalidKeyException {
        String path = "/api/v1/document/" + uuid + "/cancel";

        HttpURLConnection connection = this.connection(path, "POST");
        this.addAuthentication(connection, path, new byte[0]);

        if (connection.getResponseCode() != 200) {
            throw new RuntimeException("Could not cancel document");
        }
    }

    public void deleteDocument(String uuid) throws IOException, NoSuchAlgorithmException, InvalidKeyException {
        String path = "/api/v1/document/" + uuid;

        HttpURLConnection connection = this.connection(path, "DELETE");
        this.addAuthentication(connection, path, new byte[0]);

        if (connection.getResponseCode() != 200) {
            throw new RuntimeException("Could not delete document");
        }
    }

    public String getAuthMethods() throws IOException, NoSuchAlgorithmException, InvalidKeyException {
        String path = "/api/v1/auth/methods";

        HttpURLConnection connection = this.connection(path, "GET");

        if (connection.getResponseCode() != 200) {
            throw new RuntimeException("Could not get auth methods");
        }

        return new String(this.responseBody(connection), "UTF-8");
    }

    public String fulfillInvitation(String invitationUuid, String body) throws IOException, NoSuchAlgorithmException, InvalidKeyException {
        String path = "/api/v1/invitation/" + invitationUuid + "/signature";

        HttpURLConnection connection = this.connection(path, "POST");
        this.addAuthentication(connection, path, body.getBytes("UTF-8"));

        OutputStream outputStream = connection.getOutputStream();
        outputStream.write(body.getBytes("UTF-8"));
        outputStream.close();

        if (connection.getResponseCode() != 201) {
            throw new RuntimeException("Could not start fulfilling invitation");
        }

        return connection.getHeaderField("Location");
    }
}
