package net.visma.sign;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class App {
    private static byte[] examplePdfContent() throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        URL uri = new URL("https://sign.visma.net/empty.pdf");
        InputStream is = uri.openStream();
        byte[] byteChunk = new byte[4096];
        int n;
        while ((n = is.read(byteChunk)) > 0) {
            baos.write(byteChunk, 0, n);
        }
        return baos.toByteArray();
    }

    public static void main(String[] args) throws IOException, InvalidKeyException, NoSuchAlgorithmException {
        VismaSign vismaSign = new VismaSign("https://sign.visma.net", "organization-identifier-uuid-here", Base64.getDecoder().decode("organization-secret-here"));

        String documentUuid = vismaSign.createDocument("{\"document\":{\"name\": \"Java test\"}}");
        System.out.println("Created document " + documentUuid);

        String fileInfo = vismaSign.addFile(documentUuid, examplePdfContent(), null);
        System.out.println("Added file " + fileInfo);

        String invitations = vismaSign.createInvitations(documentUuid, "[{\"email\":\"petri.koivula@fraktio.fi\"}]");
        System.out.println("Created invitations " + invitations);

        //String invitation = vismaSign.getInvitation("449bbe29-9ffd-49ce-9525-9a34dfe21de8");
        //System.out.println("Got invitation " + invitation);

        //String document = vismaSign.getDocument("5548e5a0-64c6-4e36-b31b-da85c255afb7");
        //System.out.println("Got document " + document);

        //byte[] fileContent = vismaSign.getDocumentFile("5548e5a0-64c6-4e36-b31b-da85c255afb7");
        //System.out.println("Got document file of length " + fileContent.length);

        //vismaSign.cancelDocument(documentUuid);
        //System.out.println("Canceled document " + documentUuid);

        //vismaSign.deleteDocument(documentUuid);
        //System.out.println("Deleted document " + documentUuid);

        //String authMethods = vismaSign.getAuthMethods();
        //System.out.println("Got auth methods " + authMethods);

        //String fulfillUrl = vismaSign.fulfillInvitation("449bbe29-9ffd-49ce-9525-9a34dfe21de8", "{\"returnUrl\":\"http://return-here.tunk.io/\",\"identifier\":\"010101-123N\",\"authService\":\"tupas-sppop\"}");
        //System.out.println("Got fulfill url " + fulfillUrl);


    }
}
